#pragma once

#include "stdafx.h"
#include "DynAspecRatio.h"
#include "DynAspecRatioUI.h"
#include "SceneManager.h"
#include "ScnMain.h"

HRESULT DynAspecRatioUI::InitializeUI( void* pInitData )
{
	HRESULT retVal = NULL;

	// Initialize XUI for communication between ATG and XUI in the application
	retVal = InitShared( DynAspecRatio::getInstance().m_pd3dDevice, &(DynAspecRatio::getInstance().m_d3dpp), XuiD3DXTextureLoader);
	if( FAILED( retVal ) )
	{
		return retVal;
	}

	retVal = XuiRenderGetXuiDevice(GetDC(), &m_pXuiDevice);
	if( FAILED( retVal ) )
	{
		return retVal;
	}

	// This is just the default font, you can change it to whatever font you have
	retVal = RegisterDefaultTypeface( L"AppFont", L"file://game:/Media/Fonts/xarialuni.ttf" );
	if( FAILED( retVal ) )
	{
		return retVal;
	}
	// Accepts a LPCWSTR for the firstScene L"firstScene" defines a LPCWSTR 
	SceneManager::getInstance().SetSkin( *this, L"Main.xur" );

	return S_OK;
}

void DynAspecRatioUI::RunFrame()
{
	CXuiModule::RunFrame();	
}

void DynAspecRatioUI::DispatchXUIInput( XINPUT_KEYSTROKE* pKeystroke )
{
	XuiProcessInput(pKeystroke);
}

HRESULT DynAspecRatioUI::PreRenderUI( void * pRenderData )
{
	CXuiModule::PreRender();
	return S_OK;
}

HRESULT DynAspecRatioUI::RenderUI( D3DDevice * pDevice, int nHOverscan, int nVOverscan, UINT uWidth, UINT uHeight)
{
	XuiTimersRun();
	XuiRenderBegin( GetDC(), D3DCOLOR_ARGB( 255, 0, 0, 0) );

	D3DXMATRIX matOrigView;
	D3DXMATRIX matOrigTrans;
	XuiRenderGetViewTransform( GetDC(), &matOrigView );

	// Scale depending on width and height
	D3DXMATRIX matView;
	int NewWidth = uWidth - (nHOverscan * 2);
	int NewHeight = uHeight - (nVOverscan * 2);
	D3DXVECTOR2 vScaling = D3DXVECTOR2( NewWidth / 1280.0f, NewHeight / 720.0f );
	D3DXVECTOR2 vTranslation = D3DXVECTOR2( (float)nHOverscan, (float)nVOverscan );
	D3DXMatrixTransformation2D( &matView, NULL, 0.0f, &vScaling, NULL, 0.0f, &vTranslation);
	XuiRenderSetViewTransform( GetDC(), &matView );

	XUIMessage msg;
	XUIMessageRender msgRender;
	XuiMessageRender( &msg, &msgRender, GetDC(), 0xffffffff, XUI_BLEND_NORMAL );
	XuiSendMessage( GetRootObj(), &msg );

	XuiRenderSetViewTransform( GetDC(), &matOrigView );
	XuiRenderEnd( GetDC() );

	return S_OK;
}

HRESULT DynAspecRatioUI::UpdateUI( void * pUpdateData )
{
	RunFrame();
	return S_OK;
}

HRESULT DynAspecRatioUI::CreateMainCanvas()
{
	ASSERT( m_bXuiInited );
	if( !m_bXuiInited )
	{
		return E_UNEXPECTED;
	}

	ASSERT(m_hObjRoot == NULL );
	if( m_hObjRoot )
	{
		return E_UNEXPECTED;
	}

	HRESULT retVal = XuiCreateObject(L"XuiCanvas", &m_hObjRoot );
	if(FAILED(retVal))
	{
		return retVal;
	}

	return retVal;
}
HRESULT DynAspecRatioUI::RegisterXuiClasses()
{
	ScnMain::Register();
	return S_OK;
}
HRESULT DynAspecRatioUI::UnregisterXuiClasses()
{
	ScnMain::Unregister();
	return S_OK;
}