#pragma once

static const D3DVECTOR4 g_vClearColor = { 0.0f, 0.0f, 0.0f, 0.0f };
static const DWORD g_dwTileWidth = 1280;
static const DWORD g_dwTileHeight = 256;
static const DWORD g_dwFrameWidth = 1280;
static const DWORD g_dwFrameHeight = 720;
static const D3DRECT g_dwTiles[3] =
{
	{		0,				0, g_dwTileWidth, g_dwTileHeight },
	{		0, g_dwTileHeight, g_dwTileWidth, g_dwTileHeight * 2},
	{		0, g_dwTileHeight * 2, g_dwTileWidth, g_dwFrameHeight },
};


class DynAspecRatio : public ATG::Application
{

public:
	static DynAspecRatio& getInstance()
	{
		static DynAspecRatio singleton;
		return singleton;
	}

protected:
	DynAspecRatio() {}
	~DynAspecRatio() {}
	DynAspecRatio(const DynAspecRatio&); // Prevent copy-construction
	DynAspecRatio operator=(const DynAspecRatio&); // Prevent assignment

	//Rendering Surface and textures
	D3DSurface* m_pBackBuffer;
	D3DSurface* m_pDepthBuffer;
	D3DTexture* m_pFrontBuffer[2];
	D3DTexture* m_pFinalFrontBuffer;
	DWORD       m_dwCurFrontBuffer;

	// ATG App interface
	HRESULT Update();
	HRESULT Initialize();
	HRESULT Render();

private:
	HRESULT RenderScene();
	HRESULT CreateRenderTargets( void );
};