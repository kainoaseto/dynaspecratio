#pragma once
#include "stdafx.h"
#include "ScnMain.h"
#include "Settings.h"

ScnMain::ScnMain()
{
	curHorizOverscan = 0;
	curVertOverscan = 0;
	curLetterbox = 1;
}


HRESULT ScnMain::OnInit(XUIMessageInit *pInitData, BOOL& bHandled)
{
	InitializeChildren();
	LetterboxCheckbox.SetCheck(1);
	return S_OK;
}
HRESULT ScnMain::OnNotifyPress(HXUIOBJ hObjPressed, BOOL& bHandled)
{
	if(hObjPressed == LetterboxCheckbox)
	{
		Settings::getInstance().SetEnableLetterbox(LetterboxCheckbox.IsChecked()); 
	}
	bHandled = TRUE;
	return S_OK;
}
HRESULT ScnMain::OnNotifyValueChanged(HXUIOBJ hObjChanged, XUINotifyValueChanged* pNotifyValueChagnedData, BOOL& bHandled)
{
	if(hObjChanged == HorizOverscanSlider)
	{
		Settings::getInstance().SetHorizOverscan(pNotifyValueChagnedData->nValue);
	}
	else if(hObjChanged == VertOverscanSlider)
	{
		Settings::getInstance().SetVertOverscan(pNotifyValueChagnedData->nValue);
	}
	return S_OK;
}

HRESULT ScnMain::InitializeChildren( void )
{
	GetChildById(L"HorizOverscan", &HorizOverscanSlider);
	GetChildById(L"VertOverscan", &VertOverscanSlider);
	GetChildById(L"LetterboxCheckbox", &LetterboxCheckbox);
	return S_OK;
}