#pragma once

#include "stdafx.h"
#include "DynAspecRatioUI.h"

class SceneManager
{

public:
	static SceneManager& getInstance()
	{
		static SceneManager singleton;
		return singleton;
	}

	void SetSkin( DynAspecRatioUI& g_uiApp, LPCWSTR firstScene);
private:
	SceneManager() {}
	~SceneManager() {}

	SceneManager(const SceneManager& );
	SceneManager& operator = (const SceneManager&);

	void LoadSkin( DynAspecRatioUI& g_uiApp, LPCWSTR firstScene );

};