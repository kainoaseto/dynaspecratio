#pragma once

class Settings
{
public:
	static Settings& getInstance()
	{
		static Settings singleton;
		return singleton;
	}

	int GetHorizOverscan() { return m_HorizOverscan; }
	void SetHorizOverscan(int value) { m_HorizOverscan = value; }

	int GetVertOverscan() {	return m_VertOverscan; }
	void SetVertOverscan(int value) { m_VertOverscan = value; }

	int GetEnableLetterbox() { return m_EnableLetterbox; }
	void SetEnableLetterbox(int value) { m_EnableLetterbox = value; }

private:
	int m_HorizOverscan;
	int m_VertOverscan;
	int m_EnableLetterbox;

	Settings();
	~Settings() {}
	Settings(const Settings&);
	Settings operator=(const Settings&);
};