#pragma once
#include "stdafx.h"
#include "SceneManager.h"

void SceneManager::SetSkin( DynAspecRatioUI& g_uiApp, LPCWSTR firstScene = L"Main.xur")
{
	LoadSkin(g_uiApp, firstScene);
}

void SceneManager::LoadSkin( DynAspecRatioUI& g_uiApp, LPCWSTR firstScene)
{
	HRESULT hr = g_uiApp.LoadSkin( L"file://game:\\Skin.xzp#skin.xur" );
	if( hr != S_OK )
	{
		printf("Failed to load skin: 0x%08x\n", hr);
		return;
	}

	hr = g_uiApp.LoadFirstScene( L"file://game:\\Skin.xzp#", firstScene);
	if( hr != S_OK )
	{
		printf("Failed to load first scene: 0x%08x\n", hr);
		return;
	}
}
