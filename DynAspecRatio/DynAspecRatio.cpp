#pragma once

#include "stdafx.h"
#include "DynAspecRatio.h"
#include "DynAspecRatioUI.h"
#include "Settings.h"

// Creates the targets to render to. This is what applies Multisampling to the scenes so that when the resolution changes text
// will not look more and more pixelated as you go away from the target, along with other images.

HRESULT DynAspecRatio::CreateRenderTargets( void )
{
	HRESULT retVal;

	//Initialize D3DSurface Parameters
	D3DSURFACE_PARAMETERS ddsParams = { 0 };

	//Create Render Targets for use with Predicted Tiling
	retVal = m_pd3dDevice->CreateRenderTarget( g_dwTileWidth, g_dwTileHeight, D3DFMT_X8R8G8B8, D3DMULTISAMPLE_4_SAMPLES, 0, 0, &m_pBackBuffer, &ddsParams );
	if( retVal != D3D_OK)
	{
		printf("Failed to create Render Targets");
		return S_FALSE;
	}

	// Set up the surface size parameters
	ddsParams.Base = m_pBackBuffer->Size / GPU_EDRAM_TILE_SIZE;
	ddsParams.HierarchicalZBase = 0;

	// Create Depth Stencil Surface
	retVal = m_pd3dDevice->CreateDepthStencilSurface( g_dwTileWidth, g_dwTileHeight, D3DFMT_D24S8, D3DMULTISAMPLE_4_SAMPLES, 0, 0, &m_pDepthBuffer, &ddsParams );
	if(retVal != D3D_OK)
	{
		printf("Failed to create stencil surface");
		return S_FALSE;
	}

	// Create First Frame buffer
	retVal = m_pd3dDevice->CreateTexture( g_dwFrameWidth, g_dwFrameHeight, 1, 0, D3DFMT_LE_X8R8G8B8, 0, &m_pFrontBuffer[0], NULL );
	if(retVal != D3D_OK)
	{
		printf("Failed to create frame buffer #1");
		return S_FALSE;
	}

	// Create Second Frame Buffer
	retVal = m_pd3dDevice ->CreateTexture( g_dwFrameWidth, g_dwFrameHeight, 1, 0, D3DFMT_LE_X8R8G8B8, 0, &m_pFrontBuffer[1], NULL );
	if(retVal != D3D_OK)
	{
		printf("Failed to create frame buffer #2");
		return S_FALSE;
	}

	return S_OK;

}

// This initializes the UI

HRESULT DynAspecRatio::Initialize( void )
{
	CreateRenderTargets();

	DynAspecRatioUI::getInstance().InitializeUI(NULL);
	return S_OK;

}

HRESULT DynAspecRatio::RenderScene()
{
	m_pd3dDevice->SetRenderTarget( 0, m_pBackBuffer );
	m_pd3dDevice->SetDepthStencilSurface( m_pDepthBuffer );

	//Begin Rendering the scene and compnents
	m_pd3dDevice->BeginTiling( 0, ARRAYSIZE(g_dwTiles), g_dwTiles, &g_vClearColor, 1, 0);
	{
		//Render XUI to Device
		DynAspecRatioUI::getInstance().RenderUI( m_pd3dDevice, Settings::getInstance().GetHorizOverscan(), Settings::getInstance().GetVertOverscan(), g_dwFrameWidth, g_dwFrameHeight);
	}

	m_pd3dDevice->EndTiling( 0, NULL, m_pFrontBuffer[m_dwCurFrontBuffer], NULL, 1, 0, NULL);

	return S_OK;
}

HRESULT DynAspecRatio::Render()
{
	DynAspecRatioUI::getInstance().PreRenderUI(NULL);

	RenderScene();

	m_pd3dDevice->SynchronizeToPresentationInterval();

	m_pd3dDevice->Swap( m_pFrontBuffer[ m_dwCurFrontBuffer ], NULL );
	m_dwCurFrontBuffer = (m_dwCurFrontBuffer + 1 ) % 2;

	return S_OK;
}

HRESULT DynAspecRatio::Update()
{
	XINPUT_KEYSTROKE keyStroke;
	XInputGetKeystroke( XUSER_INDEX_ANY, XINPUT_FLAG_ANYDEVICE, &keyStroke );

	DynAspecRatioUI::getInstance().DispatchXUIInput( &keyStroke );

	DynAspecRatioUI::getInstance().UpdateUI(NULL);

	return S_OK;
}