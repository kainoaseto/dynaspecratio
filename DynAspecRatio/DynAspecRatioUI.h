#pragma once

#include "stdafx.h"

class DynAspecRatioUI : public CXuiModule
{
private:
	IXuiDevice * m_pXuiDevice;

public:
	DynAspecRatioUI() {}
	~DynAspecRatioUI() {}

	DynAspecRatioUI(const DynAspecRatioUI&);
	DynAspecRatioUI& operator=(const DynAspecRatioUI&);

	static DynAspecRatioUI& getInstance()
	{
		static DynAspecRatioUI singleton;
		return singleton;
	}

	IXuiDevice * getXuiDevice() { return m_pXuiDevice; }

	virtual void RunFrame();
	void DispatchXUIInput( XINPUT_KEYSTROKE* pKeystroke );
	HRESULT InitializeUI( void * pInitData );
	HRESULT PreRenderUI( void * pRenderData );
	HRESULT RenderUI( D3DDevice * pDevice, int nHOverscan, int nVOverscan, UINT uWidth, UINT uHeight);
	HRESULT UpdateUI( void * pUpdateData );

	HRESULT CreateMainCanvas();
	HRESULT RegisterXuiClasses();
	HRESULT UnregisterXuiClasses();
};

