// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// XUI Related Header files
#include <xtl.h>
#include <xui.h>
#include <xuiapp.h>

// ATG Header Files
#include <AtgApp.h>
#include <AtgMediaLocator.h>
#include <AtgUtil.h>

// Windows header Files
#include <d3d9.h>
#include <stdio.h>
#include <iostream>

#pragma warning(disable:4652) // Use C++ exception handling
