#pragma once
#include "stdafx.h"

class ScnMain : CXuiSceneImpl
{
public:
	ScnMain();
	~ScnMain() {}

	CXuiSlider VertOverscanSlider;
	CXuiSlider HorizOverscanSlider;
	CXuiCheckbox LetterboxCheckbox;
	int curVertOverscan;
	int curHorizOverscan;
	int curLetterbox;

	//XUI msg map

	XUI_IMPLEMENT_CLASS( ScnMain, L"ScnMain", XUI_CLASS_SCENE )

	XUI_BEGIN_MSG_MAP()
		XUI_ON_XM_INIT(OnInit)
		XUI_ON_XM_NOTIFY_PRESS( OnNotifyPress )
		XUI_ON_XM_NOTIFY_VALUE_CHANGED( OnNotifyValueChanged )
	XUI_END_MSG_MAP()

	HRESULT OnInit(XUIMessageInit *pInitData, BOOL& bHandled);
	HRESULT OnNotifyPress(HXUIOBJ hObjPressed, BOOL& bHandled);
	HRESULT OnNotifyValueChanged(HXUIOBJ hObjChanged, XUINotifyValueChanged* pNotifyValueChagnedData, BOOL& bHandled);

	HRESULT InitializeChildren( void );
};