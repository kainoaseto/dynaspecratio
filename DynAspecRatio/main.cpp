#include "stdafx.h"

#include "DynAspecRatio.h"
#include "Settings.h"
//-------------------------------------------------------------------------------------
// Name: main()
// Desc: The application's entry point
//-------------------------------------------------------------------------------------
void __cdecl main()
{
	ATG::GetVideoSettings( &(DynAspecRatio::getInstance().m_d3dpp.BackBufferWidth), &(DynAspecRatio::getInstance().m_d3dpp.BackBufferHeight) );

	DynAspecRatio::getInstance().m_d3dpp.BackBufferCount        = 1;
	DynAspecRatio::getInstance().m_d3dpp.MultiSampleType        = D3DMULTISAMPLE_4_SAMPLES;
	DynAspecRatio::getInstance().m_d3dpp.EnableAutoDepthStencil = FALSE;
	DynAspecRatio::getInstance().m_d3dpp.DisableAutoBackBuffer  = TRUE;
	DynAspecRatio::getInstance().m_d3dpp.DisableAutoFrontBuffer = TRUE;
	DynAspecRatio::getInstance().m_d3dpp.SwapEffect             = D3DSWAPEFFECT_DISCARD;
	DynAspecRatio::getInstance().m_d3dpp.PresentationInterval   = D3DPRESENT_INTERVAL_DEFAULT;

	DynAspecRatio::getInstance().m_dwDeviceCreationFlags |= D3DCREATE_BUFFER_2_FRAMES | D3DCREATE_CREATE_THREAD_ON_0;

	XVIDEO_MODE VideoMode; 
	XMemSet( &VideoMode, 0, sizeof(XVIDEO_MODE) ); 
	XGetVideoMode( &VideoMode );

	if(!VideoMode.fIsWideScreen && Settings::getInstance().GetEnableLetterbox() == 0)
	{
		DynAspecRatio::getInstance().m_d3dpp.Flags |= D3DPRESENTFLAG_NO_LETTERBOX;
	}

	DynAspecRatio::getInstance().Run();
}